# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import sys
import time
import zlib  # библиотека для сжатия
import PIL.Image  # библиотека для работы с растовой графикой
from PyQt5.QtGui import QIcon, QSurfaceFormat

from PyQt5.QtWidgets import QMainWindow, QApplication, QDialog, QTableWidgetItem, QInputDialog, QFileDialog
# noinspection PyUnresolvedReferences
from PyQt5.uic import loadUi
from PyQt5 import QtCore, uic
from glsl_syntax import Highlighter, QSurfaceFormat, QOpenGLVersionProfile
from DbShadeData import DATABASE, DbShaderData
from OpenGL.GL import *
from OpenGL.GL import shaders
import numpy as np  # библиотека для работы с массивами

from constants import QUAD_VERTICES


class MainWindow(QMainWindow):
    """
    Основной класс приложения, определяет графический интерфейс пользователя, делает начальную инициализацию OpenGL,
    запускает рендеринг(Преобразование сцены в изображение на экране)
    """
    __image_channel1 = None  # создание канала картинки
    __image_channel2 = None
    __image_channel3 = None
    __image_channel4 = None
    __g_shader_program = None
    __c_shader_program = None

    __start_time = 0
    __old_time = 0

    __window = None
    __should_update = True
    __resolution = None

    __VAO = None  # Vertex Arrays Object (VAO),говорит OpenGL, какую часть VBO следует использовать
    # в последующих командах.
    __VBO = None  # Vertex Buffer Object,озволяющее загружать определенные данные в память GPU. Например, если вы хотите
    # сообщить GPU координаты вершин, цвета или нормали, нужно создать VBO и положить эти данные в него.

    __tex_output = -1
    __paused = False

    def __init__(self, *args):
        """
        Конструктор
        """
        super(MainWindow, self).__init__(*args)  # возвращает прокси-объект, который делегирует вызовы методов
        # классу-родителю текущего класса
        loadUi("UI/Main.ui", self)
        self.__set_format()
        _ = Highlighter(self.shaderEditor)  # Инициализация редактора подсветкой синтаксиса
        self.exitMenuItem.triggered.connect(lambda event: (self.close(), sys.exit()))  # Назначаем событие закрытия
        # программы

        DATABASE.connect()  # Подключение к базе
        if not DATABASE.table_exists('dbshaderdata'):  # проверка существования таблицы
            DATABASE.create_tables([DbShaderData])  # Создание таблицы
            channel0_widht, channel0_height, channel0 = self.open_file('assets/textures/earthmab.jpg')  # передаем
            # ширину, высоту с картинки для канала по умолчанию
            infile = open('shaders/default.glsl', 'r').read()  # создание шейдера и открытие
            # Выбираем шейдер по умолчанию
            default_shader = DbShaderData(
                shader_name='default',
                shader_body=zlib.compress(infile.encode('utf-8')),
                channel0_width=channel0_widht,
                channel0_height=channel0_height,
                channel0=channel0
            )
            default_shader.save()  # Сохранение шейдера

        default_shader = DbShaderData.get(shader_name='default')  # выбор шейдера
        shader_body = zlib.decompress(default_shader.shader_body).decode('utf-8')  # распаковка
        self.shaderEditor.setPlainText(shader_body)  # Это свойство получает и устанавливает содержимое текстового
        # редактора как обычный текст.  Предыдущее содержимое удаляется, а история отмены / повтора сбрасывается
        # при установке свойства.
        self.openGLWidget.initializeGL = self.init_gl  # Здесь выполняется настройка контекста отображения.
        self.openGLWidget.paintGL = self.gl_loop  # Выводит OpenGL сцену.
        self.timer = QtCore.QTimer(self)
        self.timer.singleShot(0, lambda: self.openGLWidget.update())  # запустить одноразовый таймер
        # Назначаем события на кнопки
        self.pushButton.clicked.connect(lambda event: self.open_shader())
        self.pauseButton.clicked.connect(lambda event: self.pause())
        self.playButton.clicked.connect(lambda event: self.play())
        self.saveButton.clicked.connect(lambda event: self.save())
        self.channel1.clicked.connect(lambda event: self.select_channel1())
        self.channel2.clicked.connect(lambda event: self.select_channel2())
        self.channel3.clicked.connect(lambda event: self.select_channel3())
        self.channel4.clicked.connect(lambda event: self.select_channel4())
        self.actionOpen_Shader.triggered.connect(lambda event: self.open_shader())
        self.saveMenuItem.triggered.connect(lambda event: self.save())
        self.exitMenuItem.triggered.connect(lambda event: self.exec())

        self.pushButton.setIcon(QIcon("assets/icons/open.png"))
        self.saveButton.setIcon(QIcon("assets/icons/save.png"))
        self.playButton.setIcon(QIcon("assets/icons/play.png"))
        self.pauseButton.setIcon(QIcon("assets/icons/pause.png"))
        self.channel1.setIcon(QIcon("assets/images/pick_channel.png"))
        self.channel2.setIcon(QIcon("assets/images/pick_channel.png"))
        self.channel3.setIcon(QIcon("assets/images/pick_channel.png"))
        self.channel4.setIcon(QIcon("assets/images/pick_channel.png"))

    def save(self):
        """ Сохранение шейдера в базу данных"""
        shader_name, ok = QInputDialog.getText(self, 'Shader Name', '')
        if ok:
            self.__paused = True
            text = self.shaderEditor.toPlainText()
            new_shader = DbShaderData(
                shader_name=shader_name,
                shader_body=zlib.compress(text.encode('utf-8')),
            )
            new_shader.save()
            self.init_gl(text)
            self.__paused = False

    def pause(self):
        """
        Остановить рендеринг
        """
        self.__paused = True

    def play(self):
        """
        Возообновить рендеринг
        """
        self.__paused = False

    # noinspection PyPep8Naming
    def init_gl(self, shader_body=None):
        """
        Инициализация OpenGL
        """
        if shader_body is None:
            default_shader = open('shaders/default.glsl', 'r').read()
        else:
            default_shader = shader_body
            glDeleteVertexArrays(1, [self.__VAO])

        vertex_shader_str = open('shaders/marching.vert', 'r').read()  # считываем вершинный шейдер
        fragment_shader_str = open('shaders/marching.frag', 'r').read()  # считываем фрагментный шейдер
        compute_shader_str = open('shaders/marching.comp', 'r').read()  # считываем вычислительный шейдер

        compute_shader_str = compute_shader_str.replace('/* ---------- {MainImage} ----------*/', default_shader)

        vertex_shader = shaders.compileShader(vertex_shader_str, GL_VERTEX_SHADER)  # Компилируем вершинный шейдер
        fragment_shader = shaders.compileShader(fragment_shader_str, GL_FRAGMENT_SHADER)  # Компилируем фрагментный
        # шейдер
        compute_shader = shaders.compileShader(compute_shader_str, GL_COMPUTE_SHADER)  # Компилируем вычислительный
        # шейдер

        self.__VAO = glGenVertexArrays(1)  # Создаем VAO
        glBindVertexArray(self.__VAO)  # связывает объект массива вершин с именем array

        # Компилируем шейдеры
        self.__g_shader_program = shaders.compileProgram(vertex_shader, fragment_shader)
        self.__c_shader_program = shaders.compileProgram(compute_shader)

        # Преобразуем массив вершин в массив байт для передачи на видеокарту
        quads = np.array(QUAD_VERTICES, dtype=np.float32)

        # Vertex Buffer Object
        self.__VBO = glGenBuffers(1)  # возвращает n имен буферных объектов в буферах

        glBindBuffer(GL_ARRAY_BUFFER, self.__VBO)  # связываем объект буфера с указанной точкой привязки буфера.
        glBufferData(GL_ARRAY_BUFFER, quads.nbytes, quads, GL_STATIC_DRAW)  # Копируем буфер на видеокарту

        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * ctypes.sizeof(ctypes.c_float), None)  # Передача данных о
        # структуре вершин на видеокарту
        glEnableVertexAttribArray(0)  # Даём видеокарте знать какой индекс использовать для доступа к массиву вершин

        # создаем выходную текстуру для вычислительного шейдера
        self.__resolution = self.openGLWidget.frameGeometry().width(), self.openGLWidget.frameGeometry().height()
        self.__tex_output = glGenTextures(1)

        # Открываем картинку по умолчанию, для текстурирования объектов сцены
        self.texture_channel_with_file("assets/textures/earthmab.jpg", 1)

        # Задаем начальное время запуска в наносекундах
        self.__start_time = float(time.time_ns())

    def gl_loop(self):
        """
        Рендеринг
        """
        if not self.__should_update:
            return

        if self.__paused:
            self.timer.singleShot(100, lambda: self.openGLWidget.update())
            return

        # Используем в начале вычислительный шейдер
        glUseProgram(self.__c_shader_program)

        glActiveTexture(GL_TEXTURE0)  # Выбираем первый канал
        tex_uniform_tocation = glGetUniformLocation(self.__c_shader_program, "img_output")  # возвращает целое число,
        # которое представляет расположение определенной uniform переменной в шейдере.
        image_unit_index = 0
        glUniform1i(tex_uniform_tocation, image_unit_index)  # передаем значение в переменную шейдера

        # выбирает указанную текстуру как активную для наложения ее на объекты.
        glBindTexture(GL_TEXTURE_2D, self.__tex_output)

        # Задаем корректировку изображения для расстягивания и приближения
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, self.__resolution[0], self.__resolution[1], 0, GL_RGBA, GL_FLOAT,
                     None)

        # Привязываем текстуру к переменной шейдера
        glBindImageTexture(image_unit_index, self.__tex_output, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA32F)

        if self.__image_channel1:
            glActiveTexture(GL_TEXTURE1)
            glBindTexture(GL_TEXTURE_2D, self.__image_channel1)
            glUniform1i(glGetUniformLocation(self.__c_shader_program, "iChannel0"), 1)
        if self.__image_channel2:
            glActiveTexture(GL_TEXTURE2)
            glBindTexture(GL_TEXTURE_2D, self.__image_channel2)
            glUniform1i(glGetUniformLocation(self.__c_shader_program, "iChannel1"), 2)
        if self.__image_channel3:
            glActiveTexture(GL_TEXTURE3)
            glBindTexture(GL_TEXTURE_2D, self.__image_channel3)
            glUniform1i(glGetUniformLocation(self.__c_shader_program, "iChannel2"), 3)
        if self.__image_channel3:
            glActiveTexture(GL_TEXTURE4)
            glBindTexture(GL_TEXTURE_2D, self.__image_channel4)
            glUniform1i(glGetUniformLocation(self.__c_shader_program, "iChannel3"), 4)
        glActiveTexture(GL_TEXTURE0)

        # Передаем значение времени в переменную шейдера
        time_tocation = glGetUniformLocation(self.__c_shader_program, "iTime")
        new_time = (time.time_ns() - self.__start_time) / 1000000000.0
        glUniform1f(time_tocation, new_time)

        # Передаем значение разницы в шейдере
        delta = (new_time - self.__old_time) / 1000000.0
        self.__old_time = new_time
        delta_time_location = glGetUniformLocation(self.__c_shader_program, "iTimeDelta")
        glUniform1f(delta_time_location, delta)

        # TODO: rework as in shadertoy
        # mouse = self.openGLWidget.mapFromGlobal(QtGui.QCursor().pos())
        # glUniform4f(self.__mouse_position, mouse.x(), mouse.y(), 0, 0)

        # Задаем количество рабочих групп в вычислительном шейдере
        glDispatchCompute(int(self.__resolution[0] / 32), int(self.__resolution[1] / 32), 1)

        # Ждем отработки вычислительного шейдера
        glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT)

        # Выводим картинку на экран при помощи фрагментного шейдера
        glClearColor(0, 0, 0, 1)
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glUseProgram(self.__g_shader_program)

        # Bind the vao (which stores the VBO with all the vertices)
        glBindVertexArray(self.__VAO)

        resolution = glGetUniformLocation(self.__g_shader_program, "iResolution")
        glUniform2i(resolution, self.__resolution[0], self.__resolution[1])

        glDrawArrays(GL_TRIANGLES, 0, 6)

        if self.__should_update:
            self.timer.singleShot(0, lambda: self.openGLWidget.update())

    def __set_format(self):
        """
        Задаем минимальную используемую версию OpenGl 4.3 (Минимальная, поддерживающая вычислительные шейдеры)
        """
        glformat: QSurfaceFormat = QSurfaceFormat()
        glformat.setVersion(4, 3)
        glformat.setProfile(QSurfaceFormat.CoreProfile)
        glformat.setSamples(4)
        QSurfaceFormat.setDefaultFormat(glformat)
        self.openGLWidget.setFormat(glformat)

        vp = QOpenGLVersionProfile()
        vp.setVersion(4, 3)
        vp.setProfile(QSurfaceFormat.CoreProfile)

    @staticmethod
    def open_file(fname):
        """
        Открываем картинку
        :param fname: имя файла
        :return: Ширина, высота, массив байт в формате RGB, или RGBA, или RGBX
        """
        im = PIL.Image.open(fname)
        try:
            return im.size[0], im.size[1], im.tobytes("raw", "RGBA", 0, -1)  # пытаемся перевести картинку в rgb формат
        except (SystemError, ValueError):
            try:
                return im.size[0], im.size[1], im.tobytes("raw", "RGB", 0, -1)
            except (SystemError, ValueError):
                return im.size[0], im.size[1], im.tobytes("raw", "RGBX", 0, -1)

    def texture_channel_with_file(self, fname, channel_number):
        """
        Задаем текстуру для канала
        :param fname: имя файла
        :param channel_number: номер канала
        """

        ix, iy, image = self.open_file(fname)

        # выбираем индекс текстуры
        if channel_number == 1:
            glActiveTexture(GL_TEXTURE1)
        if channel_number == 2:
            glActiveTexture(GL_TEXTURE2)
        if channel_number == 3:
            glActiveTexture(GL_TEXTURE3)
        if channel_number == 4:
            glActiveTexture(GL_TEXTURE4)

        texture = glGenTextures(1)
        glBindTexture(GL_TEXTURE_2D, texture)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT)  # задаем параметры текстур
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, ix, iy, 0, GL_RGB, GL_UNSIGNED_BYTE, image)

        # Задаем текстуру для канала
        if channel_number == 1:
            self.__image_channel1 = texture
        if channel_number == 2:
            self.__image_channel2 = texture
        if channel_number == 3:
            self.__image_channel3 = texture
        if channel_number == 4:
            self.__image_channel4 = texture

        glActiveTexture(GL_TEXTURE0)

    def get_image_channel(self, channel_number):

        """
        Выбор файла текстуры для канала
        :param channel_number: номер канала
        """

        self.__should_update = False

        # noinspection PyArgumentList
        fname, _ = QFileDialog.getOpenFileName(self, 'Open texture',
                                               'assets/textures', "Image files (*.jpg *.png)")
        if fname:
            self.texture_channel_with_file(fname, channel_number)
            if channel_number == 1:
                self.channel1.setIcon(QIcon(fname))
            elif channel_number == 2:
                self.channel2.setIcon(QIcon(fname))
            elif channel_number == 3:
                self.channel3.setIcon(QIcon(fname))
            elif channel_number == 4:
                self.channel4.setIcon(QIcon(fname))
        self.__should_update = True
        self.timer.singleShot(0, lambda: self.openGLWidget.update())

    def select_channel1(self):
        """
        Выбрать 1 канал
        """
        if self.__image_channel1:
            glDeleteTextures(1, [self.__image_channel1])
            self.__image_channel1 = None
        self.get_image_channel(1)

    def select_channel2(self):
        """
        Выбрать 2 канал
        """
        if self.__image_channel2:
            glDeleteTextures(1, [self.__image_channel2])
            self.__image_channel2 = None
        self.get_image_channel(2)

    def select_channel3(self):
        """
        Выбрать 3 канал
        """
        if self.__image_channel3:
            glDeleteTextures(1, [self.__image_channel3])
            self.__image_channel3 = None
        self.get_image_channel(3)

    def select_channel4(self):
        """
        Выбрать 4 канал
        """
        if self.__image_channel4:
            glDeleteTextures(1, [self.__image_channel4])
            self.__image_channel4 = None
        self.get_image_channel(4)

    # Выбор шейдера

    def set_shader(self, selection_model):
        """
        Выбор шейдера
        :param selection_model: выбранные данные по шейдеру
        """
        if selection_model.hasSelection():
            shader_name = selection_model.selectedRows()[0].data(0)
            shader = DbShaderData.select().where(DbShaderData.shader_name == shader_name).get()
            if shader is not None:
                self.__paused = True
                self.shaderEditor.setPlainText(zlib.decompress(shader.shader_body).decode('utf-8'))
                self.init_gl(self.shaderEditor.toPlainText())
                self.__paused = False

    def open_shader(self):
        """
        Создаем диалоговое окно для просмотра и выбора шейдера
        """
        # noinspection PyArgumentList
        dlg = QDialog(self)
        uic.loadUi("ui/select_shader.ui", dlg)
        dlg.setWindowTitle("Select Shader")
        shaders_list = DbShaderData.select()
        length = shaders_list.count()
        dlg.shadersList.setColumnCount(1)
        dlg.shadersList.setRowCount(length)
        for num, shader in enumerate(shaders_list):
            dlg.shadersList.setItem(num, 0, QTableWidgetItem(shader.shader_name))
        dlg.shadersList.horizontalHeader().setStretchLastSection(True)
        dlg.buttonsBox.buttons()[0].clicked.connect(lambda event: (
            selection_model := dlg.shadersList.selectionModel(),
            self.set_shader(selection_model)
        ))
        dlg.exec()

    def __del__(self):
        """
        Деструктор класса
        """
        DATABASE.close()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec_())
