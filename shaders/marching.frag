#version 330 core
out vec4 gl_FragColor;

uniform sampler2D screenTexture;
uniform ivec2 iResolution;

void main() {
    gl_FragColor = texture(screenTexture, gl_FragCoord.xy / iResolution.xy);
}

