# define PI 3.14159265358979323846

struct Material {
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    float high_light;
};

struct Object {
    Material material;
};

struct Light {
    vec3 direction;
    float occlusion;
};

/*
Singed distance field for sphere
position: position in 3D space to check if it hits th sphere
center: center of the sphere
radius: radius of the sphere
*/
float sphereSDF(in vec3 position, in vec3 center, float radius)
{
    return length(position - center) - radius;
}

/*
Place all objects in 3D space here
position: point to check an intersection with any object
*/
float world(in vec3 position)
{
    float sphere = sphereSDF(position, vec3(0.0, 0.0, 5.0), 1.0);
    return sphere;
}

/**
* gradient:
*    [
*        f(x+ε,y,z)−f(x−ε,y,z)
*        f(x,y+ε,z)−f(x,y−ε,z)
*        f(x,y,z+ε)−f(x,y,z−ε)
*    ]
*/
vec3 calculate_normal(in vec3 p)
{
    const vec3 small_step = vec3(0.001, 0.0, 0.0);

    float gradient_x = world(p + small_step.xyy) - world(p - small_step.xyy);
    float gradient_y = world(p + small_step.yxy) - world(p - small_step.yxy);
    float gradient_z = world(p + small_step.yyx) - world(p - small_step.yyx);

    return normalize(vec3(gradient_x, gradient_y, gradient_z));

}

float atan2(in float y, in float x)
{
    bool s = (abs(x) > abs(y));
    return mix(PI/2.0 - atan(x,y), atan(y,x), s);
}

vec3 spherical_texturing(in vec3 normal, in sampler2D text) {
    float u = 0.5 + atan(normal.z, normal.x) / PI + iTime / 16.0;
    float v = 0.5 + asin(normal.y) / PI;
    return texture(text, vec2(u, v)).xyz;
}

vec3 blinn_phong(vec3 current_position, Material material, Light light, vec3 rd) {

    vec3 color = vec3(0.15);
    vec3 gamma = vec3(.8);

    vec3 normal = calculate_normal(current_position);
    light.occlusion = 0.5 + 0.5 * normal.y;

    float ambient = clamp(0.5 + 0.5 * normal.y, 0.0, 1.0);
    ambient = clamp(0.5 + 0.5 * normal.y, 0.0, 1.0);

    float diffuse = clamp(dot(light.direction, normal), 0.0, 1.0);
    vec3 half_way = normalize(-rd + light.direction);
    float specular = pow(clamp(dot(half_way, normal), 0.0, 1.0), material.high_light);

    color = ambient * material.ambient * light.occlusion;
    color += diffuse * material.diffuse * light.occlusion;
    color += diffuse * specular * material.specular * light.occlusion;
    color *= spherical_texturing(normal, iChannel0);

    return pow(color, gamma);
}

/*
Raymarching algorithm
ro: ray origin
rd: ray direction
*/
vec4 ray_march(in vec3 ro, in vec3 rd)
{
    float total_distance_traveled = 0.0;
    const int NUMBER_OF_STEPS = 32;
    const float MINIMUM_HIT_DISTANCE = 0.001;
    const float MAXIMUM_TRACE_DISTANCE = 1000.0;

    for (int i = 0; i < NUMBER_OF_STEPS; ++i)
    {
        // Calculate our current position along the ray
        vec3 current_position = ro + total_distance_traveled * rd;

        // We wrote this function earlier in the tutorial -
        // assume that the sphere is centered at the origin
        // and has unit radius
        float distance_to_closest = world(current_position);

        // We hit something! Return result color
        if (distance_to_closest < MINIMUM_HIT_DISTANCE) {

            Material material;
            material.ambient = vec3(0.07, 0.07, 0.07);
            material.diffuse = vec3(0.9, 0.9, 0.9);
            material.specular = vec3(0.45, 0.45, 0.45);
            material.high_light = 25.0;

            Light light;
            light.direction = normalize(vec3(1.0, 0.2, 0.0));

            return vec4(blinn_phong(current_position, material, light, rd), 1.);

        }

        // Got to maximum distance
        if (total_distance_traveled > MAXIMUM_TRACE_DISTANCE) break;

        // accumulate the distance traveled thus far
        total_distance_traveled += distance_to_closest;
    }

    // If we get here, we didn't hit anything so just
    // return a background color (black)
    return vec4(0.0);
}

void mainImage(out vec4 fragColor, in vec2 fragCoord) {

    // Converting all coordinates (x,y) to be in the range [0,1]
    vec2 uv = fragCoord.xy / iResolution.xy;

    // Now convert from coordinate range [0,1] to [-1,1]
    // This moves pixel (0,0) to the middle of the image
    uv = uv.xy * 2.0 - 1.0;

    //the following line is necessary since the image may not be a square
    //removing this line results in the x dimension being stretched
    uv.x *= (iResolution.x/iResolution.y);

    // The position of this pixel in 3D space
    //(imagine a plane centered at the origin perpendicular to the camera,
    // what would be the 3d coordinates of this pixel?)
    vec3 pixelPos = vec3(uv.xy, 0);

    // The camera position in this example is fixed.
    // Some distance in front of the screen (sometimes called focal distance)
    vec3 cameraPos = vec3(0, 0, -5.0);

    // The ray direction is ray from the camera through the pixel
    vec3 rayDir = normalize(pixelPos - cameraPos);

    fragColor = ray_march(cameraPos, rayDir);

}