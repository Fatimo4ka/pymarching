from PyQt5.QtGui import *
from PyQt5.QtCore import *



# https://github.com/art1415926535/PyQt5-syntax-highlighting/blob/master/syntax_pars.p
# reworked for glsl

def syntax_format(color, style=''):
    """
    Return a QTextCharFormat with the given attributes.
    """
    _color = QColor()
    if type(color) is not str:
        _color.setRgb(color[0], color[1], color[2])
    else:
        _color.setNamedColor(color)

    _format = QTextCharFormat()
    _format.setForeground(_color)
    if 'bold' in style:
        _format.setFontWeight(QFont.Bold)
    if 'italic' in style:
        _format.setFontItalic(True)

    return _format


# Syntax styles that can be shared by all languages

STYLES = {
    'type': syntax_format([50, 120, 200], 'bold'),
    'keyword': syntax_format([200, 120, 50], 'bold'),
    'operator': syntax_format([150, 150, 150]),
    'brace': syntax_format('darkGray'),
    'comment': syntax_format([128, 128, 128]),
    'numbers': syntax_format([100, 150, 190]),
    'function': syntax_format([175, 0, 255]),
    'builtin': syntax_format([255, 50, 100]),
}


class Highlighter(QSyntaxHighlighter):
    """Syntax highlighter for the Python language.
    """
    # glsl keywords

    keywords = [
        'attribute', 'const', 'uniform', 'varying', 'layout', 'centroid',
        'flat', 'smooth', 'noperspective', 'patch', 'sample', 'break',
        'continue', 'do', 'for', 'while', 'switch', 'case',
        'default', 'if', 'else', 'subroutine', 'in', 'out',
        'inout', 'true', 'false', 'invariant', 'discard',
        'return', 'lowp', 'mediump', 'highp', 'precision', 'struct'
    ]

    # glsl types

    types = [
        'float', 'double', 'int', 'void', 'bool', 'mat2', 'mat3', 'mat4',
        'dmat2', 'dmat3', 'dmat4', 'mat2x2', 'mat2x3', 'mat2x4',
        'dmat2x2', 'dmat2x3', 'dmat2x4', 'mat3x2', 'mat3x3', 'mat3x4',
        'dmat3x2', 'dmat3x3', 'dmat3x4', 'mat4x2', 'mat4x3', 'mat4x4',
        'dmat4x2', 'dmat4x3', 'dmat4x4', 'vec2', 'vec3', 'vec4',
        'ivec2', 'ivec3', 'ivec4', 'uint', 'bvec2', 'bvec3', 'bvec4',
        'dvec2', 'dvec3', 'dvec4', 'sampler1D', 'sampler2D', 'sampler3D',
        'samplerCube', 'sampler1DShadow', 'sampler2DShadow', 'samplerCubeShadow',
        'sampler1DArray', 'sampler2DArray', 'sampler1DArrayShadow', 'sampler2DArrayShadow',
        'isampler1D', 'isampler2D', 'isampler3D', 'isamplerCube', 'isampler1DArray',
        'isampler2DArray', 'usampler1D', 'usampler2D', 'usampler3D', 'usamplerCube',
        'usampler1DArray', 'usampler2DArray', 'sampler2DRect', 'sampler2DRectShadow',
        'isampler2DRect', 'usampler2DRect', 'samplerBuffer', 'isamplerBuffer', 'usamplerBuffer',
        'sampler2DMS', 'isampler2DMS', 'usampler2DMS', 'sampler2DMSArray', 'isampler2DMSArray',
        'usampler2DMSArray', 'samplerCubeArray', 'samplerCubeArrayShadow', 'isamplerCubeArray',
        'usamplerCubeArray'
    ]

    # glsl operators

    operators = [
        '=',
        # Comparison
        '==', '!=', '<', '<=', '>', '>=',
        # Arithmetic
        '\+', '-', '\*', '/', '//', '\%', '\*\*',
        # In-place
        '\+=', '-=', '\*=', '/=', '\%=',
        # Bitwise
        '\^', '\|', '\&', '\~', '>>', '<<',
    ]

    # glsl braces
    braces = [
        '\{', '\}', '\(', '\)', '\[', '\]',
    ]

    #  functions
    functions = [
        'acos', 'acosh', 'asin', 'asinh', 'atan', 'atanh', 'cos', 'cosh', 'degrees', 'radians', 'sin', 'sinh', 'tan',
        'tanh', 'abs', 'ceil', 'clamp', 'dFdx', 'dFdy', 'exp', 'exp2', 'floor', 'floor', 'fma', 'fract', 'fwidth',
        'inversesqrt', 'isinf', 'isnan', 'log', 'log2', 'max', 'min', 'mix', 'mod', 'modf', 'noise', 'pow', 'round',
        'roundEven', 'sign', 'smoothstep', 'sqrt', 'step', 'trunc', 'floatBitsToInt', 'frexp', 'intBitsToFloat',
        'ldexp', 'packDouble2x32', 'packHalf2x16', 'packUnorm', 'unpackDouble2x32', 'unpackHalf2x16', 'unpackUnorm',
        'cross', 'distance', 'dot', 'equal', 'faceforward', 'length', 'normalize', 'notEqual', 'reflect', 'refract',
        'all', 'any', 'greaterThan', 'greaterThanEqual', 'lessThan', 'lessThanEqual', 'not', 'EmitStreamVertex',
        'EmitVertex', 'EndPrimitive', 'EndStreamPrimitive', 'interpolateAtCentroid', 'interpolateAtOffset',
        'interpolateAtSample', 'texelFetch', 'texelFetchOffset', 'texture', 'textureGather', 'textureGatherOffset',
        'textureGatherOffsets', 'textureGrad', 'textureGradOffset', 'textureLod', 'textureLodOffset', 'textureOffset',
        'textureProj', 'textureProjGrad', 'textureProjGradOffset', 'textureProjLod', 'textureProjLodOffset',
        'textureProjOffset', 'textureQueryLevels', 'textureQueryLod', 'textureSamples', 'textureSize', 'determinant',
        'groupMemoryBarrier', 'inverse', 'matrixCompMult', 'outerProduct', 'transpose', 'bitCount', 'bitfieldExtract',
        'bitfieldInsert', 'bitfieldReverse', 'findLSB', 'findMSB', 'uaddCarry', 'umulExtended', 'usubBorrow',
        'imageAtomicAdd', 'imageAtomicAnd', 'imageAtomicCompSwap', 'imageAtomicExchange', 'imageAtomicMax',
        'imageAtomicMin', 'imageAtomicOr', 'imageAtomicXor', 'imageLoad', 'imageSamples', 'imageSize', 'imageStore',
        'atomicAdd', 'atomicAnd', 'atomicCompSwap', 'atomicCounter', 'atomicCounterDecrement', 'atomicCounterIncrement',
        'atomicExchange', 'atomicMax', 'atomicMin', 'atomicOr', 'atomicXor', 'barrier', 'groupMemoryBarrier',
        'memoryBarrier', 'memoryBarrierAtomicCounter', 'memoryBarrierBuffer', 'memoryBarrierImage',
        'memoryBarrierShared'
    ]

    builtins = [
        'gl_ClipDistance', 'gl_CullDistance', 'gl_FragCoord', 'gl_FragDepth', 'gl_FrontFacing', 'gl_GlobalInvocationID',
        'gl_HelperInvocation', 'gl_InstanceID', 'gl_InvocationID', 'gl_Layer', 'gl_LocalInvocationID',
        'gl_LocalInvocationIndex', 'gl_NumSamples', 'gl_NumWorkGroups', 'gl_PatchVerticesIn', 'gl_PointCoord',
        'gl_PointSize', 'gl_Position', 'gl_PrimitiveID', 'gl_PrimitiveIDIn', 'gl_SampleID', 'gl_SampleMask',
        'gl_SampleMaskIn', 'gl_SamplePosition', 'gl_TessCoord', 'gl_TessLevelInner', 'gl_TessLevelOuter',
        'gl_VertexID', 'gl_ViewportIndex', 'gl_WorkGroupID', 'gl_WorkGroupSize'
    ]

    def __init__(self, document):
        QSyntaxHighlighter.__init__(self, document)

        rules = []

        # Keyword, operator, and brace rules
        rules += [(r'\b%s\b' % w, 0, STYLES['type'])
                  for w in Highlighter.types]
        rules += [(r'\b%s\b' % w, 0, STYLES['keyword'])
                  for w in Highlighter.keywords]
        rules += [(r'%s' % o, 0, STYLES['operator'])
                  for o in Highlighter.operators]
        rules += [(r'%s' % b, 0, STYLES['brace'])
                  for b in Highlighter.braces]
        rules += [(r'%s' % o, 0, STYLES['function'])
                  for o in Highlighter.functions]
        rules += [(r'%s' % o, 0, STYLES['builtin'])
                  for o in Highlighter.builtins]

        # All other rules
        rules += [
            # From '//' until a newline
            (r'//[^\n]*', 0, STYLES['comment']),

            # Numeric literals
            (r'\b[+-]?[0-9]+[lL]?\b', 0, STYLES['numbers']),
            (r'\b[+-]?0[xX][0-9A-Fa-f]+[lL]?\b', 0, STYLES['numbers']),
            (r'\b[+-]?[0-9]+(?:\.[0-9]+)?(?:[eE][+-]?[0-9]+)?\b', 0, STYLES['numbers']),
        ]

        # Build a QRegExp for each pattern
        self.rules = [(QRegExp(pat), index, fmt)
                      for (pat, index, fmt) in rules]

    # noinspection PyPep8Naming,PyShadowingBuiltins
    def highlightBlock(self, text):
        """Apply syntax highlighting to the given block of text.
        """
        # Do other syntax formatting
        for expression, nth, format in self.rules:
            index = expression.indexIn(text, 0)

            while index >= 0:
                # We actually want the index of the nth match
                index = expression.pos(nth)
                length = len(expression.cap(nth))
                self.setFormat(index, length, format)
                index = expression.indexIn(text, index + length)

        # check comments
        start = 0
        state = self.previousBlockState()
        for i in range(0, len(text)):
            if state == InsideCStyleComment:
                if text[i:i + 2] == "*/":
                    state = NormalState
                    self.setFormat(start, i - start + 2, Qt.lightGray)
            else:
                if text[i:i + 2] == "//":
                    self.setFormat(i, len(text) - i, Qt.lightGray)
                    break
                elif text[i:i + 2] == "/*":
                    start = i
                    state = InsideCStyleComment
        if state == InsideCStyleComment:
            self.setFormat(start, len(text) - start, Qt.lightGray)
        self.setCurrentBlockState(state)


NormalState = -1
InsideCStyleComment = -2
