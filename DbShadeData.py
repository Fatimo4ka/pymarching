from peewee import *

# Задаем файл базы данных Sqlite
DATABASE = SqliteDatabase('db/shaders.db')


class DbShaderData(Model):
    """
    Класс описания таблицы шейдера
    """
    class Meta:
        database = DATABASE

    shader_name = TextField(unique=True, index=True)  # Имя шейдера
    shader_body = BlobField()  # Текст шейдера

    channel0 = BlobField(null=True, default=None)  # Текстура первого канала
    channel0_width = IntegerField(null=True, default=None)  # Ширина текстуры 1 канала
    channel0_height = IntegerField(null=True, default=None)  # Высота текстуры 1 канала
    channel1 = BlobField(null=True, default=None)
    channel1_width = IntegerField(null=True, default=None)
    channel1_height = IntegerField(null=True, default=None)
    channel2 = BlobField(null=True, default=None)
    channel2_width = IntegerField(null=True, default=None)
    channel2_height = IntegerField(null=True, default=None)
    channel3 = BlobField(null=True, default=None)
    channel3_width = IntegerField(null=True, default=None)
    channel3_height = IntegerField(null=True, default=None)



